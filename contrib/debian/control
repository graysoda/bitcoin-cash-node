Source: bitcoin-cash-node
Section: utils
Priority: optional
Maintainer: Andrea Suisani <sickpig@gmail.com>
Uploaders: Andrea Suisani <sickpig@gmail.com>
Build-Depends: debhelper,
 devscripts,
 automake,
 libtool,
 bash-completion,
 libboost-system-dev (>> 1.58) | libboost-system1.58-dev,
 libdb5.3++-dev,
 libssl-dev,
 libevent-dev,
 pkg-config,
 libminiupnpc8-dev | libminiupnpc-dev (>> 1.6),
 libboost-filesystem-dev (>> 1.58) | libboost-filesystem1.58-dev,
 libboost-program-options-dev (>> 1.58) | libboost-program-options1.58-dev,
 libboost-thread-dev (>> 1.58) | libboost-thread1.58-dev,
 libboost-test-dev (>> 1.58) | libboost-test1.58-dev,
 qt4-qmake, libqt4-dev,
 qttools5-dev-tools, qttools5-dev,
 libqrencode-dev,
 libprotobuf-dev, protobuf-compiler,
 libzmq3-dev,
 python
Standards-Version: 3.9.2
Homepage: https://bitcoincashnode.org/
Vcs-Git: https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node.git
Vcs-Browser: https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node

Package: bitcoind
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: peer-to-peer network based digital currency - daemon
 Bitcoin Cash is a new digital currency that enables instant
 payments to anyone, anywhere in the world. Bitcoin uses peer-to-peer
 technology to operate with no central authority: managing transactions
 and issuing money are carried out collectively by the network. Bitcoin Cash Node
 is the name of the open source software which enables the use of this currency.
 .
 This package provides the daemon, bitcoind, and the CLI tool
 bitcoin-cli to interact with the daemon.

Package: bitcoin-qt
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: peer-to-peer network based digital currency - Qt GUI
 Bitcoin Cash is a new digital currency that enables instant
 payments to anyone, anywhere in the world. Bitcoin uses peer-to-peer
 technology to operate with no central authority: managing transactions
 and issuing money are carried out collectively by the network. Bitcoin Cash Node
 is the name of the open source software which enables the use of this currency.
 .
 This package provides Bitcoin-Qt, a GUI for Bitcoin based on Qt.

Package: bitcoin-tx
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: peer-to-peer digital currency - standalone transaction tool
 Bitcoin Cash is a new digital currency that enables instant
 payments to anyone, anywhere in the world. Bitcoin uses peer-to-peer
 technology to operate with no central authority: managing transactions
 and issuing money are carried out collectively by the network. Bitcoin Cash Node
 is the name of the open source software which enables the use of this currency.
 .
 This package provides bitcoin-tx, a command-line transaction creation
 tool which can be used without a bitcoin daemon.  Some means of
 exchanging minimal transaction data with peers is still required.
